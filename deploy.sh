rm -rf ~/.cache/helm

export HELM_EXPERIMENTAL_OCI=1
helm registry login -u gitlab+deploy-token-334223 registry.gitlab.com/kballadev


export CHARTS_FOLDER=helm/planet-express-charts/charts
rm -fr $CHARTS_FOLDER/*
helm chart pull registry.gitlab.com/kballadev/planet-express-hawkeye:0.0.23
helm chart export registry.gitlab.com/kballadev/planet-express-hawkeye:0.0.23 -d $CHARTS_FOLDER
helm chart pull registry.gitlab.com/kballadev/planet-express-abe:0.0.25
helm chart export registry.gitlab.com/kballadev/planet-express-abe:0.0.25 -d $CHARTS_FOLDER
helm chart pull registry.gitlab.com/kballadev/planet-express-heimdall:0.0.1
helm chart export registry.gitlab.com/kballadev/planet-express-heimdall:0.0.1 -d $CHARTS_FOLDER

helm repo rm `helm repo list|awk '{ print $1}'|tail +2`
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add equinor-charts https://equinor.github.io/helm-charts/charts/



cd helm/planet-express-charts
kubectl apply -f registry-credentials.yml
helm repo up
helm dep up
cd ..
helm upgrade --install planet-express-charts planet-express-charts 
