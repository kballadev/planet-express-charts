# planet-express-charts
Kubernetes charts

## Requiriments

- bitnami and neo4j helm charts
```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add equinor-charts https://equinor.github.io/helm-charts/charts/
```



## Deploy

### Required
- Before deploying we need to create the secrets to get access to the gitlab registry containers

```bash
kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=<REGISTRY_USER> --docker-password=<REGISTRY_PASSWORD>
```

### Deploying
- Running the deploy
```bash
cd helm/planet-express
helm repo up
helm dependencies update
cd ..
helm install planet-express planet-express
```