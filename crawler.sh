rm -rf ~/.cache/helm

export HELM_EXPERIMENTAL_OCI=1
helm registry login -u gitlab+deploy-token-334223 registry.gitlab.com/kballadev


export CHARTS_FOLDER=helm/planet-express-charts-crawler/charts
rm -fr $CHARTS_FOLDER/*
helm chart pull registry.gitlab.com/kballadev/planet-express:0.0.11
helm chart export registry.gitlab.com/kballadev/planet-express:0.0.11 -d $CHARTS_FOLDER
helm chart pull registry.gitlab.com/kballadev/planet-express-42:0.0.4
helm chart export registry.gitlab.com/kballadev/planet-express-42:0.0.4 -d $CHARTS_FOLDER

# helm repo rm `helm repo list|awk '{ print $1}'|tail +2`
# helm repo add bitnami https://charts.bitnami.com/bitnami
# helm repo add equinor-charts https://equinor.github.io/helm-charts/charts/



cd helm/planet-express-charts-crawler
kubectl apply -f registry-credentials.yml
helm repo up
helm dep up
cd ..
helm upgrade --install planet-express-charts-crawler planet-express-charts-crawler --set planet-express.crawler.name=ccsenet --set planet-express.crawler.source=ccsenet/ccsenet --set planet-express-42.queue.in=ccsenet
